# Whitespace breaker plugin support for MfGames Writing (Javascript).

This is a pipeline module which converts long spans of `em`, `i`, `strong`, and `b` tags to handle word wrapping. This is to get around a bug with WeasyPrint and wrapping with long italics.

This plugin is used in the `pipeline` elements inside a `publication.yaml` file.

```yaml
contents:
  - element: chapter
    number: 1
    directory: chapters
    source: /^chapter-\d+.markdown$/
    start: true
    page: 1
    pipeline: &pipelines
      - module: "@mfgames-writing/whitespace-breaker"
```
