import * as expect from "expect";
import { ContentArgs, ContentData, EditionArgs, EditionData, PublicationArgs } from "@mfgames-writing/contracts";
import "mocha";
import { WhitespaceBreakerPlugin } from "../WhitespaceBreakerPlugin";

describe("single line", function()
{
    it("single", function(done)
    {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "one <em>two three</em> four";

        // Create the plugin and process data using it.
        const plugin = new WhitespaceBreakerPlugin();
        let promise = plugin.process(contentArgs);

        promise.then(c =>
        {
            expect(c.text).toEqual("one <em>two</em> <em>three</em> four");
            done();
        });
    });

    it("pair", function(done)
    {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "one <em>two three</em> four <em>five six</em> seven";

        // Create the plugin and process data using it.
        const plugin = new WhitespaceBreakerPlugin();
        let promise = plugin.process(contentArgs);

        promise.then(c =>
        {
            expect(c.text).toEqual("one <em>two</em> <em>three</em> four <em>five</em> <em>six</em> seven");
            done();
        });
    });
});

describe("multi line", function()
{
    it("single", function(done)
    {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "one <em>two\nthree</em> four";

        // Create the plugin and process data using it.
        const plugin = new WhitespaceBreakerPlugin();
        let promise = plugin.process(contentArgs);

        promise.then(c =>
        {
            expect(c.text).toEqual("one <em>two</em>\n<em>three</em> four");
            done();
        });
    });

    it("pair", function(done)
    {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "one <em>two\nthree</em>\nfour\n<em>five\nsix</em> seven";

        // Create the plugin and process data using it.
        const plugin = new WhitespaceBreakerPlugin();
        let promise = plugin.process(contentArgs);

        promise.then(c =>
        {
            expect(c.text).toEqual("one <em>two</em>\n<em>three</em>\nfour\n<em>five</em>\n<em>six</em> seven");
            done();
        });
    });
});
