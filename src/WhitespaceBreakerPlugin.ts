import { ContentArgs, ContentPipeline } from "@mfgames-writing/contracts";

export class WhitespaceBreakerPlugin implements ContentPipeline
{
    public process(content: ContentArgs): Promise<ContentArgs>
    {
        return new Promise<ContentArgs>((resolve) =>
        {
            content.text = this.break("em", content.text);
            content.text = this.break("i", content.text);
            content.text = this.break("strong", content.text);
            content.text = this.break("b", content.text);

            resolve(content);
        });
    }

    private break(tag: string, input: string): string
    {
        // We have to match open and close tags.
        const open = "<" + tag + ">";
        const close = "</" + tag + ">";

        // JavaScript doesn't handle multi-line replacements well
        // so we change it into a unlikely character sequence, do
        // the replacements, and then change it back.
        return input
            .replace(/\n/g, "\u2764")
            .replace(/\r/g, "\u2765")
            .replace(
                new RegExp(open + "(.*?)" + close, "g"),
                (_, capture) =>
                {
                    const broken = capture
                        .replace(
                            /([\s\u2764\u2765]+)/g,
                            close + "$1" + open);

                    return open + broken + close;
                })
            .replace(/\u2764/g, "\n")
            .replace(/\u2765/g, "\r");
    }
}
