import { WhitespaceBreakerPlugin } from "./WhitespaceBreakerPlugin";

export function loadWhitespaceBreakerPlugin(
    args: any,
): WhitespaceBreakerPlugin
{
    return new WhitespaceBreakerPlugin();
}

export default loadWhitespaceBreakerPlugin;
